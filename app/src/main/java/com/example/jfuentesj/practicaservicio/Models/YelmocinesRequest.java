package com.example.jfuentesj.practicaservicio.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by josealbertofuentesjaramillo on 30/03/18.
 */

public class YelmocinesRequest {

    @SerializedName("country_code")
    private String countryCode;

    private int cities;

    @SerializedName("include_cinemas")
    private boolean includeCinemas;

    @SerializedName("include_movies")
    private boolean includeMovies;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public int getCities() {
        return cities;
    }

    public void setCities(int cities) {
        this.cities = cities;
    }

    public boolean isIncludeCinemas() {
        return includeCinemas;
    }

    public void setIncludeCinemas(boolean includeCinemas) {
        this.includeCinemas = includeCinemas;
    }

    public boolean isIncludeMovies() {
        return includeMovies;
    }

    public void setIncludeMovies(boolean includeMovies) {
        this.includeMovies = includeMovies;
    }
}
